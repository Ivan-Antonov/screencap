#!/usr/bin/env bash

USERNAME="ivan"
BUCKET_PATH="s3://dreamsai-screencaps"

install_script()
{
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
	if [[ -n $(command -v brew) ]]; then
	    echo "brew installed"
	else
	    git clone https://github.com/Homebrew/brew ~/.linuxbrew/Homebrew
	    mkdir ~/.linuxbrew/bin
	    ln -s ~/.linuxbrew/Homebrew/bin/brew ~/.linuxbrew/bin
	    eval $(~/.linuxbrew/bin/brew shellenv)
	fi	    
    elif [[ "$OSTYPE" == "darwin"* ]] ; then
	if [[ -n $(command -v brew) ]]; then
	    echo "brew installed"
	else
	    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
	fi
    fi
    
    cat requirements.txt | while read package
    do
	if [[ -n $(command -v brew) ]]; then
	    echo "$package installed"
	else
	    brew install $package
	fi
    done
}

stop_screencap()
{
    kill $(ps aux | grep ffmpeg |  awk '{print $2}')
    
    echo "Finished screencap and dumping to AWS 3"
    if [[ $DUMP != false ]]; then
	aws_dump
    fi

    exit 0
}

aws_dump()
{
    aws s3 cp "$FILE_NAME" "${BUCKET_PATH}/${USERNAME}/"
}

#### Main Script ####
while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
	install)
	    install_script
	    exit 0
	    ;;
	-f|--file)
	    FILE="$2"
	    shift # past argument
	    shift # past value
	    ;;
	-d|--dump)
	    DUMP="$2"
	    shift # past argument
	    shift # past value
	    ;;
	*)    # unknown option
	    ;;
    esac
done


if [[ $DUMP == only ]]; then
    FILE_NAME="${FILE}"
    aws_dump
else
    trap 'stop_screencap' 2
    DATUM="$(date "+%Y-%m-%d_%H-%M-%S")"
    FILE_NAME="${DATUM}_${USERNAME}_${FILE}"
    ffmpeg -f avfoundation -i "1:1" $FILE_NAME
fi



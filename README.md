# Overview #
Setup automatic screencapturing and pushing to AWS S3.

- [**Linux/Mac**](#linuxmac) bash shell script
- [**Windows**](#windows) bat script


| Field                 | Value     |
| --------------------- | --------- |
| AWS Access Key ID     | ????      |
| AWS Secret Access Key | ????      |
| Default region name   | ap-east-1 |
| Default output format | json      |

## Experienced Users (Linux/Mac) ##

### Install aws client
```
apt-get install awscli
brew install awscli
```

- Configure aws using table above

```
aws configure
```

### Upload file to AWS
```
aws s3 cp "<file_name>" "s3://dreamsai-screencaps/<user_name>"
```

## Linux/Mac ##
- Gitclone project to the local machine
- Make script executable
- Run the installation

``` shell
git clone https://username@gitlab.com/Ivan-Antonov/screencap.git
chmod +X screencap.sh
source screencap.sh install
```

- Configure AWS using table above

``` shell
aws configure
```

- In `screencap.sh` change username to own name
- Optionally change the AWS S3 Bucket Path

``` shell
vim screencap.sh
```

``` text
USERNAME="ivan"
BUCKET_PATH="s3://dreamsai-screencaps"
```

- Restart the terminal window

### Options ###

* `-f/--filename`: set the filename to save or push to AWS S3 (make sure the filename ends in .mkv). The datetime and user are automatically added during screecapping.
* `-d/--dump`: set to **false** if you don't want to push to AWS S3 server. Set to **only** if you only want to dump to AWS S3 server.

``` shell
source screencap.sh -f file.mkv -d false
```

### Common Linux/Mac Bugs ###
* **Error** `ffmpeg` input is incorrect
- Run the following code and change the inputs in `screencap.sh`
``` shell
ffmpeg -f avfoundation -list_devices true -i ""
```

## Windows ## 
- Run command-prompt with administrator privelages
- Gitclone project to the local machine
- Run the installation
``` shell
git clone https://username@gitlab.com/Ivan-Antonov/screencap.git
screencap.bat install
```

- - Configure AWS using table above

``` shell
aws configure
```

- In `screencap.sh` change username to own name
- Optionally change the AWS S3 Bucket Path
``` text
USERNAME="ivan"
BUCKET_PATH="s3://dreamsai-screencaps"
```

- Restart command prompt

### Options ###

* `-f/--filename`: set the filename to save or push to AWS S3 (make sure the filename ends in .mkv). The datetime and user are automatically added during screecapping.
* `-d/--dump`: set to **false** if you don't want to push to AWS S3 server. Set to **only** if you only want to dump to AWS S3 server.

``` shell
screencap.bat -f file.mkv -d false
```

### Common Windows Bugs ###
* **Error** `ffmpeg` input is incorrect
- Run the following code and change the inputs in `screencap.bat`
``` shell
ffmpeg -list_devices true -f dshow -i dummy
```

* **Error** Naming incorrect/ No such `ffmpeg` arguement
- Restart the command prompt as the environmental variable `%date%` has a bug (is not pure)


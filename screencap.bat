set USERNAME=ivan
set BUCKET_PATH=s3://dreamsai-screencaps/

@set argC=0
for %%x in (%*) do @set /A argC+=1

set arg1=%1
if %argC% gtr 1 set arg2=%2
if %argC% EQU 4 set arg3=%3
if %argC% EQU 4 set arg4=%4

@set "FILE="
@set "DUMP=true"

if x%arg1% == xinstall (call :install_script) else (call :normal_run)

exit /B %ERRORLEVEL%

:install_script
ffmpeg -version >nul 2>&1 && (
echo ffmpeg installed
) || (
cd C:\
curl "https://ffmpeg.zeranoe.com/builds/win64/static/ffmpeg-latest-win64-static.zip" -LOJ
tar -xf ffmpeg-latest-win64-static.zip
rename ffmpeg-latest-win64-static FFMPEG
setx path "%PATH%;C:\FFMPEG\bin"
del ffmpeg-latest-win64-static.zip
)

aws --version >nul 2>&1 && (
echo aws installed
) || (
curl "https://s3.amazonaws.com/aws-cli/AWSCLISetup.exe" -LOJ
AWSCLISetup.exe
del AWSCLISetup.exe
echo done
)
EXIT /B 0

:first_pass

if /i x%arg1% == x-f (
set FILE=%arg2%
) 
if /i x%arg1% == x-d (
set DUMP=%arg2%
)

exit /B 0

:second_pass

if %argC% EQU 4 (
if x%arg3% == x-f (
set FILE=%arg4%
) else (
set FILE=%arg2%))

if %argC% EQU 4 (
if /i x%arg3% == x-d (
set DUMP=%arg4%
)else (
set DUMP=%arg2%))

exit /B 0

:normal_run

call :first_pass
call :second_pass

if not x%DUMP% == xonly (
call :date_time
set FILE_NAME=%DATESTR%_%USERNAME%_%FILE%
call :ffmpeg_run
) else (
set FILE_NAME=%FILE%
)

if not %DUMP% == false (
call :aws_dump
) else (
echo "not sending to aws"
)

EXIT /B 0

:ffmpeg_run
ffmpeg -f dshow -i video="UScreenCapture" %FILE_NAME%
taskkill /im ffmpeg.exe /t /f
EXIT /B 0

:aws_dump
aws s3 cp %FILE_NAME% %BUCKET_PATH%/%USERNAME%
EXIT /B 0

:date_time

for /f "tokens=1-4 delims=/ " %%i in ("%date%") do (
     set day=%%i
     set month=%%j
     set year=%%k
)
for /f "tokens=1-4 delims=: " %%i in ("%time%") do (
     set hours=%%i
     set minutes=%%j
     set seconds=%%k
)
set DATESTR=%year%-%month%-%day%_%hours%-%minutes%-%seconds:~0,-3%
EXIT /B 0
